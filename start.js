let state = 0;

function updateState(state, action) {
    if (action.type === 'INC'){
        return state + action.amount;
    } else if (action.type === 'DEC'){
        return state - action.amount;
    } else {
        return state;
    }
}

class Store{
    constructor(updateState, state){
        this._updateState = updateState;
        this._state = state;
    }

    update(action){
        this._state = this._updateState(this._state, action);
    }
}

const store = new Store(updateState(), 0);

const incAction = {type: 'INC', amount: 5};
const decAction = {type: 'DEC', amount: 3};


state = updateState(state, incAction);
console.log(state);

state = updateState(state, decAction);
console.log(state);

state = updateState(state, {});
console.log(state);